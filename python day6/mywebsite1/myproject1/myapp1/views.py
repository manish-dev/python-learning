from django.shortcuts import render
from django.http import HttpResponse
import time
import random

# Create your views here.
homeLink = "<a href='/myapp1'>Home</a><br>"

def hellofunction(request):
    return HttpResponse("hello world, first view<br>"+homeLink) 

def welcometodjango(request):
    return HttpResponse("Welcome to django server<br>"+homeLink)

def showdateandtime(request):
    datetime=time.asctime(time.localtime())
    return HttpResponse(datetime+"<br>"+homeLink)

def printmynamefunction(request):
    nos= random.randrange(1,10)
    if (nos==0):
        nos=1
    msg=''
    for i in range(nos):
        msg = msg + 'manish <br>'
    return HttpResponse(msg+"<br>"+homeLink)

def welcome1function(request,username):
    msg=''
    if (username=='Guru'):
        msg=username+' is teaching'
    else:
        msg=username+' is learning'
    return HttpResponse(msg+"<br>"+homeLink)

def welcomefunctionQ(request):
    city=request.GET['city']
    if (city=='Bangalore'):
        resp="<body bgcolor='green'>"
    else:
        resp="<body bgcolor='red'>"
    return HttpResponse(resp+"<br>You are living in "+city+"<br>"+homeLink)

def homefunction(request):
    msg="<h1> Home Page </h1>"
    msg=msg+"<a href='hello'>Hello</a><br>"
    msg=msg+"<a href='welcome'>Welcome</a><br>"
    msg=msg+"<a href='randname'>Print My Name</a><br>"
    msg=msg+"<a href='showdate'>Show Date and Time</a><br>"
    msg=msg+"<a href='welcome1/Harish'>Path parameter example</a><br>"
    msg=msg+"<a href='welcomefunctionQ?city=Bangalore'>Query Parameter example</a><br>"
    return HttpResponse(msg)

