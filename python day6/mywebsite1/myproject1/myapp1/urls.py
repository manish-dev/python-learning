from django.urls import path
from . import views

urlpatterns = [
    path('hello', views.hellofunction, name='hello'),
    path('welcome', views.welcometodjango, name='welcome'),
    path('showdate', views.showdateandtime, name='showdate'),
    path('randname', views.printmynamefunction, name='randname'),
    path('welcome1/<str:username>',views.welcome1function, name='welcome1'),
    path('welcomefunctionQ',views.welcomefunctionQ,name='qp'),
    path('',views.homefunction,name='home'),
]