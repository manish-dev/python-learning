from functools import reduce;
import empModule
import productModule

def fn1():
    "This will print the name of the employee whose salary is the maximum"
    allEmps=empModule.emps()
    emp=reduce(lambda x,y: x if x['salary'] > y['salary'] else y,allEmps)
    print(emp['name'], ' and ',emp['salary'])

def fn2():
    "This will print the sum of prices of all products"
    allProds=productModule.prods()
    obj=reduce((lambda x,y:{"price":x['price']+y['price']}),allProds)
    print("Total prices of all products ",obj["price"])

def fn3():
    "this will print minimum employee salary"
    allEmps=empModule.emps()
    emp=reduce(lambda x,y: x if x['salary'] < y['salary'] else y,allEmps)
    print(emp['name'], ' and ',emp['salary'])

def fn4():
    allEmps=empModule.emps()
    emp=map(lambda x: {"name": x["name"],"salary": x["salary"] + x["salary"]*.10},allEmps)
    print(list(emp))

def fn5():
    allProds=productModule.prods()
    obj=filter((lambda x:{"price":x['price'] > 100}),allProds)
    print(list(obj))